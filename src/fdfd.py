import matplotlib.pyplot as plt
from scipy.sparse.linalg import eigs, spsolve
from scipy.linalg import eig, norm, solve
import numpy as np
import numpy.matlib
import scipy.sparse as sp
from copy import deepcopy


def fdfd(spec, eps, mu, cond_num, pad, phase):

    eps0 = deepcopy(eps)
    mu0 = deepcopy(mu)

    def extend(eps, pad):

        eps = np.vstack((np.matlib.repmat(eps[0, :], pad[0], 1), eps, np.matlib.repmat(eps[-1, :], pad[1], 1)))
        eps = np.hstack((np.matlib.repmat(eps[:, 0].reshape(-1, 1), 1,  pad[2]),
                         eps, np.matlib.repmat(eps[:, -1].reshape(-1, 1), 1, pad[3])))

        return eps

    def stretched(dims, shift, dir, thickness, sigma, order):
        if (dir == 'x'):
            pos = np.arange(dims[0]) + 1 + shift
            pos = np.maximum(0, thickness - np.minimum(pos - 1, dims[0] - pos))
            A = np.matlib.repmat(pos.reshape(-1, 1), 1, dims[1])

        if (dir == 'y'):
            pos = np.arange(dims[1]) + 1 + shift
            pos = np.maximum(0, thickness - np.minimum(pos - 1, dims[1] - pos))
            A = np.matlib.repmat(pos.reshape(1, -1), dims[0], 1)

        S = np.float_power((np.ones(dims) + 1j * sigma * np.float_power((A/thickness), order)), -1)
        S = sp.spdiags(S.flatten(order='F'), 0, np.prod(dims), np.prod(dims))

        return S

    def A_matrices(dims, t_pml, sigma_pml, exp_pml, bc, eps, mu, p, q, spec, cond_num, phase):
        N = np.prod(dims)
        C1 = np.ones((N, 1))
        C2 = np.ones((N, 1))

        for i in range(N):
            if (np.mod(i+1, dims[0]) == 1):
                C2[i, 0] = 0

        A_TM = [None] * cond_num
        A_TE = [None] * cond_num
        E_TM = [None] * cond_num
        H_TE = [None] * cond_num

        for i in range(cond_num):
            C3 = np.vstack((C2[1:], [0]))
            C4 = np.vstack((np.zeros((dims[0], 1)), np.ones((N - dims[0], 1))))
            C5 = np.vstack((np.ones((N - dims[0], 1)), np.zeros((dims[0], 1))))
            C6 = np.vstack((phase[i] * np.ones((dims[0], 1)), np.zeros((N - dims[0], 1))))
            C7 = np.vstack((np.zeros((N - dims[0], 1)), -phase[i] * np.ones((dims[0], 1))))

            DxE = sp.spdiags(np.hstack((-C1, C2)).T, np.array([0, 1]), N, N)
            DxH = sp.spdiags(np.hstack((-C3, C2)).T, np.array([-1, 0]), N, N)
            DyE = sp.spdiags(np.hstack((C6, -C1, C4)).T, np.array([-(N - dims[0]), 0, dims[0]]), N, N)
            DyH = sp.spdiags(np.hstack((-C5, C1, C7)).T, np.array([-dims[0], 0, (N - dims[0])]), N, N)

            epsX = sp.spdiags(0.5 * np.hstack((C1, C2)).T, np.array([0, 1]), N, N)
            epsY = sp.spdiags(0.5 * np.hstack((C1, C4)).T, np.array([0, dims[0]]), N, N)
            epsZ = sp.spdiags(C1.T, 0, N, N)

            muX = epsY
            muY = epsX
            muZ = 0.5 * (epsX + epsY)

            def sc(shift, dir): return stretched(dims, shift, dir, t_pml, sigma_pml, exp_pml)

            TM = [None] * 4
            TE = [None] * 4

            if (bc == 'pml'):
                TM[0] = sp.hstack(((-sc(0, 'y') @ DyE), (sc(0, 'x') @ DxE)))
                TM[2] = sp.vstack(((sc(0, 'y') @ DyH), (-sc(0, 'x') @ DxH)))
                TE[0] = sp.hstack(((-sc(0, 'y') @ DyH), (sc(0, 'x') @ DxH)))
                TE[2] = sp.vstack(((sc(0, 'y') @ DyE), (-sc(0, 'x') @ DxE)))
            if (bc == 'per'):
                TM[0] = sp.hstack(((-DyE), (sc(0, 'x') @ DxE)))
                TM[2] = sp.vstack(((DyH), (-sc(0, 'x') @ DxH)))
                TE[0] = sp.hstack(((-DyH), (sc(0, 'x') @ DxH)))
                TE[2] = sp.vstack(((DyE), (-sc(0, 'x') @ DxE)))

            TM[1] = sp.vstack(((epsX), (epsY)))
            TM[3] = muZ
            TE[1] = sp.vstack(((muX), (muY)))
            TE[3] = epsZ

            def my_diag(z): return sp.spdiags(z, 0, z.size, z.size)

            # Physics operator in terms of x
            A_TM[i] = TM[0] @ my_diag(TM[1] @ p) @ TM[2] - (spec[i]["omega"]**2) * my_diag(TM[3] @ mu)
            A_TE[i] = TE[0] @ my_diag(TE[1] @ p) @ TE[2] - (spec[i]["omega"]**2) * my_diag(TE[3] @ eps)
            E_TM[i] = my_diag(TM[1] @ p) @ TM[2]
            H_TE[i] = my_diag(TE[1] @ p) @ TE[2]
            # A_TM[i] = TM[0] @ my_diag(TM[1] * p) @ TM[2] -  my_diag(TM[3] @ mu)
            # omega = spec[i]["omega"]

        return A_TE, A_TM, E_TM, H_TE

    def create_system(t_pml, pml_top, cond_num, spec, A_TM, A_TE, E_TM, H_TE, dims, pad):
        # We will use the formula b = (qA - Aq)*f_src
        in_pos = t_pml + 1
        t2_pml = t_pml * pml_top

        b = [None] * cond_num
        q = [None] * cond_num
        A = [None] * cond_num
        G = [None] * cond_num

        for i in range(cond_num):

            b[i] = np.zeros(dims).astype(np.complex)
            q[i] = np.zeros(dims).astype(np.complex)

            # Determine q and b depending on where the source comes from
            beta = spec[i]["in"]["beta"]
            if (spec[i]["mode_type"] == 'TM'):
                field = spec[i]["in"]["H"][:, 2]
                A[i] = A_TM[i]
                G[i] = E_TM[i]
            # TODO: put else:
            for k in range(in_pos + 2):
                if (spec[i]["in"]["side"] == 'left'):
                    # pass
                    b[i][k, t2_pml + pad[2]: -(t2_pml + pad[3])] = field * np.exp((k+1) * 1j * beta)
                    if (k < in_pos):
                        q[i][k, t2_pml + pad[2]: -(t2_pml + pad[3])] = 1

                if (spec[i]["in"]["side"] == 'right'):
                    b[i][-(k+1), t2_pml + pad[2]: -(t2_pml + pad[3])] = field * np.exp((k + 1) * 1j * beta)
                    if (k < in_pos):
                        q[i][-(k+1), t2_pml + pad[2]: -(t2_pml + pad[3])] = 1

                if (spec[i]["in"]["side"] == 'bottom'):
                    b[i][t2_pml + pad[0]: -(t2_pml + pad[1]), k] = field * np.exp((k + 1) * 1j * beta)
                    if (k < in_pos):
                        q[i][t2_pml + pad[0]: -(t2_pml + pad[1]), k] = 1

                if (spec[i]["in"]["side"] == 'top'):
                    b[i][t2_pml + pad[0]: -(t2_pml + pad[1]), -(k+1)] = field * np.exp((k + 1) * 1j * beta)
                    if (k < in_pos):
                        q[i][t2_pml + pad[0]: -(t2_pml + pad[1]), -(k+1)] = 1

            b[i] = b[i].flatten(order='F')
            q[i] = q[i].flatten(order='F')

            # b[i] = b[i] * q[i]

            b[i] = q[i] * (A[i] @ b[i]) - A[i] @ (q[i] * b[i])

        return A, b, G

    # -------------
    # Start here
    # --------------

    omega = spec[0]["omega"]
    bc = spec[0]["bc"]

    # Hard coded parameters for the pml
    t_pml = 20                  # Thickness of pml
    sigma_pml = 1 / omega       # Strength of pml
    exp_pml = 2.5               # Exponential spatial increase in pml strength

    # Expand eps to include room for the pml padding
    pml_top = int(bc == 'pml')
    eps = extend(eps0, t_pml * np.array([1, 1, pml_top, pml_top]))
    mu = extend(mu0, t_pml * np.array([1, 1, pml_top, pml_top]))
    dims = eps.shape

    eps = eps.flatten(order='F')
    mu = mu.flatten(order='F')
    p = np.float_power(eps, -1)
    q = np.float_power(mu, -1)

    A_TE, A_TM, E_TM, H_TE = A_matrices(dims, t_pml, sigma_pml, exp_pml, bc, eps, mu, p, q, spec, cond_num, phase)

    # i = 0
    # spec[i]["in"]["side"] = 'bottom'

    A, b, G = create_system(t_pml, pml_top, cond_num, spec, A_TM, A_TE, E_TM, H_TE, dims, pad)

    # Solve for the fields
    F = [None] * cond_num
    F2 = [None] * cond_num
    for i in range(cond_num):
        F[i] = spsolve(A[i], b[i])
        F2[i] = (1j / spec[i]["omega"]) * G[i] @ F[i]

    # Field processing
    Fx = [None] * cond_num
    Fy = [None] * cond_num
    Fz = [None] * cond_num

    for i in range(cond_num):
        Fx[i] = F2[i][:np.prod(dims)].reshape(dims, order='F')
        Fy[i] = F2[i][-np.prod(dims):].reshape(dims, order='F')
        Fz[i] = F[i].reshape(dims, order='F')

        if (bc == 'pml'):
            Fx[i] = Fx[i][t_pml:-t_pml, t_pml:-t_pml]
            Fy[i] = Fy[i][t_pml:-t_pml, t_pml:-t_pml]
            Fz[i] = Fz[i][t_pml:-t_pml, t_pml:-t_pml]
        if (bc == 'per'):
            Fx[i] = Fx[i][t_pml:-t_pml, :]
            Fy[i] = Fy[i][t_pml:-t_pml, :]
            Fz[i] = Fz[i][t_pml:-t_pml, :]

    return Fx, Fy, Fz

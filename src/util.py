import matplotlib.pyplot as plt
import numpy as np
from copy import deepcopy

from wgmode import wgmode

# Functions


def make_wg(eps_around, mu_around, wg, w, s, p, ew, mw, dims):
    eps = eps_around * np.ones(dims)
    mu = mu_around * np.ones(dims)
    for k in range(len(wg)):
        i = wg[k] - 1
        if (s[i] == 'left'):
            eps[:2, int(p[i] - w[i]/2):int(p[i] + w[i]/2)] = ew[i]
            mu[:2, int(p[i] - w[i]/2):int(p[i] + w[i]/2)] = mw[i]
        if (s[i] == 'right'):
            eps[-2:, int(p[i] - w[i]/2):int(p[i] + w[i]/2)] = ew[i]
            mu[-2:, int(p[i] - w[i]/2):int(p[i] + w[i]/2)] = mw[i]
        if (s[i] == 'top'):
            eps[int(p[i] - w[i]/2):int(p[i] + w[i]/2), -2:] = ew[i]
            mu[int(p[i] - w[i]/2):int(p[i] + w[i]/2), -2:] = mw[i]
        if (s[i] == 'bottom'):
            eps[int(p[i] - w[i]/2):int(p[i] + w[i]/2), :2] = ew[i]
            mu[int(p[i] - w[i]/2):int(p[i] + w[i]/2), :2] = mw[i]

    return eps, mu


def mode_plot(H, E):
    fig, axs = plt.subplots(2, 3, sharex=True, sharey=True, dpi=150)
    # myplot(axs[0,0], np.real(H[:, 0]))
    axs[0, 0].plot(np.real(H[:, 0]))
    axs[0, 0].set_title('Hx')
    axs[0, 1].plot(np.real(H[:, 1]))
    axs[0, 1].set_title('Hy')
    axs[0, 2].plot(np.real(H[:, 2]))
    axs[0, 2].set_title('Hz')
    axs[1, 0].plot(np.real(E[:, 0]))
    axs[1, 0].set_title('Ex')
    axs[1, 1].plot(np.real(E[:, 1]))
    axs[1, 1].set_title('Ey')
    axs[1, 2].plot(np.real(E[:, 2]))
    axs[1, 2].set_title('Ez')


def myplot(ax, EH):
    ax.plot(EH)
    ax.set_ylim([-1, 1])
    ax.set_title('Hx')


# Temp variables
# Design parameters
omega = [0.4, 0.4]
resolution = 'H'
bc = 'pml'

# Design parameters
dims = np.array([50, 40])
eps = {
    "around": 1.0,
    "inside": 2.3,
    "lims": np.array([1, 2.3])
}
mu = {
    "vary": 'No',
    "around": 1.0,
    "inside": 1.0,
    "lims": np.array([1, 1])
}
# Maximum number of iteration
num_iters = 10

# Waveguide parameters
spec_wg = {
    "num": 2,
    "side": ['left', 'right'],
    "size": [16, 16],
    "pos": [20, 20],
    "eps": [2.3, 2.3],
    "mu": [1, 1]
}

cond = [None] * 1
cond[0] = [1, 2, 1, 1, 'TM', 1, 64]
# cond[1] = [1, 3, 1, 3, 'TM', 1, 64]

spec_wg["cond"] = cond
spec_wg["angle"] = [[0, 0]]
spec_wg["ncond"] = len(cond)

params = {
    "pad": [10, 10, 10, 10],
    "mon": 10
}

eps0 = deepcopy(eps)
mu0 = deepcopy(mu)


def pcolor(v, line=0):
    fig, ax = plt.subplots(1, 1, dpi=100)
    vmax = np.max(np.real(v))
    vmin = np.min(np.real(v))
    if (line == 1):
        im = ax.pcolor(v, cmap='jet', vmin=vmin, vmax=vmax, edgecolors='k', linewidths=0.5)
    else:
        im = ax.pcolor(v, cmap='jet', vmin=vmin, vmax=vmax)
    ax.axis('equal')
    fig.colorbar(im)


def imshow(v):
    fig, ax = plt.subplots(1, 1, dpi=120)
    vmax = np.max(np.real(v))
    vmin = np.min(np.real(v))
    im = ax.pcolor(v, cmap='jet', vmin=vmin, vmax=vmax)
    ax.axis('equal')
    fig.colorbar(im)
#     plt.colorbar()

# def pcolor(v):
#     vmax = np.max(np.abs(v))
#     vmin = np.min(np.abs(v))
#     plt.pcolor(v, cmap='jet', vmin=-vmax, vmax=vmax)
#     plt.axis('equal')
#     plt.colorbar()
# --------------------------
# -----------------
# Real code starts from here
# -----------------


n = spec_wg["ncond"]
# n = 1

# spec_wg
num = spec_wg["num"]
width = spec_wg["size"]
side = spec_wg["side"]
pos = spec_wg["pos"]
eps_wg = spec_wg["eps"]
mu_wg = spec_wg["mu"]
angle = spec_wg["angle"]

side

specs = [None] * (n+1)

for i in range(n):
    inout = [spec_wg["cond"][i][0], spec_wg["cond"][i][1]]
    eps, mu = make_wg(eps0["around"], mu0["around"], inout, width, side, pos, eps_wg, mu_wg, dims)
    # Create specification structure
    specs[i] = {
        "omega": omega[i],
        "bc": bc,
        "eps0": eps,
        "mu0": mu,
        "in": {"side": side[spec_wg["cond"][i][0] - 1], "mode": spec_wg["cond"][i][2]},
        "out": {"side": side[spec_wg["cond"][i][1] - 1], "mode": spec_wg["cond"][i][3]},
        "mode_type": spec_wg["cond"][i][4],
        "weight": spec_wg["cond"][i][5],
        "nPhase": spec_wg["cond"][i][6]
    }

    # ----------------
    # Calculate the input and output mode
    # ----------------
    # Calcualte input
    specs[i]["in"]["beta"], H0in, E0in, specs[i]["in"]["H"], specs[i]["in"]["E"], specs[i]["P_in"], phase, Hci, Eci = wgmode(
        omega[i], eps, mu, specs[i]["in"]["side"], specs[i]["in"]["mode"], specs[i]["mode_type"], angle[i][0], 'in')

    if (bc == 'pml'):
        phase = 0

    specs[i]["phase"] = phase

    # Calculate output
    specs[i]["out"]["beta"], H0out, E0out, specs[i]["out"]["H"], specs[i]["out"]["E"], specs[i]["P_out"], phase, Hco, Eco = wgmode(
        omega[i], eps, mu, specs[i]["out"]["side"], specs[i]["out"]["mode"], specs[i]["mode_type"], angle[i][1], 'in')

    # Plot the input and output modes
    # mode_plot(H0in, E0in)
    # mode_plot(H0out, E0out)

    # print(spec_wg["cond"][i][1])

eps, mu = make_wg(eps0["around"], mu0["around"], np.arange(num) + 1, width, side, pos, eps_wg, mu_wg, dims)

eps[2:-2, 2:-2] = eps0["inside"]
mu[2:-2, 2:-2] = mu0["inside"]

if (bc == 'per'):
    eps[2:-2, :] = eps0["inside"]
    mu[2:-2, :] = mu0["inside"]

# specs[n]["eps0"] = eps
# specs[n]["mu0"] = mu

specs[n] = {
    "eps": eps,
    "mu": mu,
    "params": params
}

# mode_plot(H0in, E0in)

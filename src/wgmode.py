import matplotlib.pyplot as plt
from scipy.sparse.linalg import eigs
from scipy.linalg import eig, norm
import numpy as np
import scipy.sparse as sp
from copy import deepcopy


def wgmode(omega, eps0, mu0, side, mode, type, agle, dir):
    eps, mu = make_side(side, eps0, mu0)
    phase = 0
    N = eps.size
    G = np.zeros((N, 1))
    # index correction
    mode = mode - 1

    if (type == 'TM'):
        eps_x, eps_y, eps_xy = interpolation_inv(eps)
        mu_x, mu_y, mu_xy = interpolation(mu)
    else:
        eps_x, eps_y, eps_xy = interpolation(eps)
        mu_x, mu_y, mu_xy = interpolation_inv(mu)

    # BUild derivative csr_matrix
    D = sp.spdiags([-np.ones((N)), np.ones(N)], [-1, 0], N, N)
    D = sp.lil_matrix(D)
    D[0, N-1] = -1
    D = D.tocsr()

    # Build matrix for eigenvaluye problem
    def my_diag(x): return sp.spdiags(x.reshape(-1), 0, N, N)

    if (type == 'TM'):
        if (side == 'left' or side == 'right'):
            A = my_diag(eps_y) * (-D.transpose() @ my_diag(np.float_power(eps_x, -1)) @ D + omega**2 * my_diag(mu_xy))
        else:
            A = my_diag(eps_x) * (-D.transpose() @ my_diag(np.float_power(eps_y, -1)) @ D + omega**2 * my_diag(mu_xy))
    else:
        if (side == 'left' or side == 'right'):
            A = my_diag(mu_x) * (-D @ my_diag(np.float_power(mu_y, -1)) @ D.transpose() + omega**2 * my_diag(eps))
        else:
            A = my_diag(mu_y) * (-D @ my_diag(np.float_power(mu_x, -1)) @ D.transpose() + omega**2 * my_diag(eps))

    # Remove the wrap-around elements (we don't want periodic boundaries).
    A = sp.lil_matrix(A)
    A[N-1, 0] = 0
    A[0, N-1] = 0
    A = A.tocsr()

    if (mode + 1 > 0):
        D1, V = eig(A.todense())
        ind = np.argsort(-np.real(D1))
        beta = np.sqrt(D1[ind].astype(np.complex))

        # Sort the mode profiles.
        F = V[:, ind]

        # Orient modes so that the largest magnitude element is always positive.
        ch = np.diag((np.amax(F, axis=0) > np.amax(-F, axis=0)).astype(int) - (np.amax(F, axis=0) <=
                                                                               np.amax(-F, axis=0)).astype(int))

        F = F @ ch

        # Coefficient
        c = 1
        if (((side == 'left') and (dir == 'out')) or ((side == 'right') and (dir == 'in')) or ((side == 'bottom') and (dir == 'out')) or ((side == 'top') and (dir == 'in'))):
            c = -1

        # Calculate E / H.
        if (type == 'TM'):
            if ((side == 'left') or (side == 'right')):
                F1 = (-1j / omega) * my_diag(np.float_power(eps_x.reshape(-1), -1)) @ D @ F
                F2 = ((-c / omega) * my_diag(np.float_power(eps_y.reshape(-1), -1)) @ F) @ my_diag(beta)
            else:
                F1 = (-1j / omega) * my_diag(np.float_power(eps_y.reshape(-1), -1)) @ D @ F
                F2 = ((-c / omega) * my_diag(np.float_power(eps_x.reshape(-1), -1)) @ F) @ my_diag(beta)

        # Normalize power (to unity).
        power = np.diag(-c*F2.T @ F)

        F = F @ np.diag(np.float_power(power, -0.5))
        F1 = F1 @ np.diag(np.float_power(power, -0.5))
        F2 = F2 @ np.diag(np.float_power(power, -0.5))

        beta = beta[mode]
        F = F[:, mode]
        F1 = F1[:, mode]
        F2 = F2[:, mode]

    err = norm(A @ F - beta**2 * F)
    power = np.sum(np.real(np.multiply(F2, np.conj(F))))

    # Create E0 and H0 for display
    if (type == 'TM'):
        H0 = np.hstack((np.zeros((N, 1)), np.zeros((N, 1)), F.reshape(-1, 1)))
        if (side == 'left' or side == 'right'):
            E0 = np.hstack((np.imag(F1.reshape(-1, 1)), F2.reshape(-1, 1), np.zeros((N, 1))))
        else:
            E0 = np.hstack((F2.reshape(-1, 1), np.imag(F1.reshape(-1, 1)), np.zeros((N, 1))))

    # Add a phase term to Hy (TE, horz), Hx (TE, vert), Ex (TM, horz), Ey (TM, vert)
    c = 1
    if (type == 'TM'):
        c = -1

    if (((side == 'left' or side == 'bottom') and dir == 'in') or ((side == 'right' or side == 'top') and dir == 'out')):
        F2 = F2 * np.exp(1j * c * 0.5 * beta)
    else:
        F2 = F2 * np.exp(-1j * c * 0.5 * beta)

    # Create second layer for non lens (G1, G2)
    if (mode != -2):
        coeff = 1

        if (dir == 'out'):
            coeff = -1

        G = F * np.exp(coeff * 1j * beta)
        G1 = F1 * np.exp(coeff * 1j * beta)
        G2 = F2 * np.exp(coeff * 1j * beta)

    # Create E and H
    if (type == 'TM'):

        H = np.hstack((np.zeros((N, 1)), np.zeros((N, 1)), F.reshape(-1, 1)))
        Hc = np.hstack((np.zeros((N, 1)), np.zeros((N, 1)), G.reshape(-1, 1)))

        if (side == 'left' or side == 'right'):
            E = np.hstack((F1.reshape(-1, 1), F2.reshape(-1, 1), np.zeros((N, 1))))
            Ec = np.hstack((G1.reshape(-1, 1), G2.reshape(-1, 1), np.zeros((N, 1))))
        else:
            E = np.hstack((F2.reshape(-1, 1), F1.reshape(-1, 1), np.zeros((N, 1))))
            Ec = np.hstack((G2.reshape(-1, 1), G1.reshape(-1, 1), np.zeros((N, 1))))

    return beta, H0, E0, H, E, power, phase, Hc, Ec


def make_side(side, eps, mu):

    eps0 = deepcopy(eps)
    mu0 = deepcopy(mu)
    if (side == 'left'):
        eps_side = eps0[0, :].reshape(1, -1)
        mu_side = mu0[0, :].reshape(1, -1)
    if (side == 'right'):
        eps_side = eps0[-1, :].reshape(1, -1)
        mu_side = mu0[-1, :].reshape(1, -1)
    if (side == 'top'):
        eps_side = eps0[:, -1].reshape(-1, 1)
        mu_side = mu0[:, -1].reshape(-1, 1)
    if (side == 'bottom'):
        eps_side = eps0[:, 0].reshape(-1, 1)
        mu_side = mu0[:, 0].reshape(-1, 1)

    return eps_side, mu_side


def interpolation_inv(eps):

    shape = eps.shape

    if (shape[0] == 1 and shape[1] > 1):
        # left and right
        eps_x = np.float_power(0.5 * (np.float_power(eps, -1) + np.float_power(eps, -1)), -1)
        eps_y = np.float_power(0.5 * (np.float_power(eps, -1) + np.float_power(np.roll(eps, -1, axis=1), -1)), -1)
        eps_xy = np.float_power(0.5 * (np.float_power(eps, -1) + np.float_power(np.roll(eps, -1, axis=1), -1)), -1)

        return eps_x, eps_y, eps_xy

    if (shape[0] > 1 and shape[1] == 1):
        # top and bottom
        eps_x = np.float_power(0.5 * (np.float_power(eps, -1) + np.float_power(np.roll(eps, -1, axis=0), -1)), -1)
        eps_y = np.float_power(0.5 * (np.float_power(eps, -1) + np.float_power(eps, -1)), -1)
        eps_xy = np.float_power(0.5 * (np.float_power(eps, -1) + np.float_power(np.roll(eps, -1, axis=0), -1)), -1)

        return eps_x, eps_y, eps_xy

    if (shape[0] > 1 and shape[1] > 1):
        eps_x = np.float_power(0.5 * (np.float_power(eps, -1) + np.float_power(np.roll(eps, -1, axis=0), -1)), -1)
        eps_y = np.float_power(0.5 * (np.float_power(eps, -1) + np.float_power(np.roll(eps, -1, axis=1), -1)), -1)
        eps_xy = np.float_power(0.5 * (np.float_power(eps, -1) + np.float_power(np.roll(eps, -1), -1)), -1)

        return eps_x, eps_y, eps_xy


def interpolation(eps):

    shape = eps.shape

    if (shape[0] == 1 and shape[1] > 1):
        # left and right
        eps_x = 0.5 * (eps + eps)
        eps_y = eps_y = 0.5 * (eps + np.roll(eps, -1, axis=1))
        eps_xy = eps_y = 0.5 * (eps + np.roll(eps, -1, axis=1))

        return eps_x, eps_y, eps_xy

    if (shape[0] > 1 and shape[1] == 1):
        # left and right
        eps_x = 0.5 * (eps + np.roll(eps, -1, axis=0))
        eps_y = eps_y = 0.5 * (eps + eps)
        eps_xy = eps_y = 0.5 * (eps + np.roll(eps, -1, axis=0))

        return eps_x, eps_y, eps_xy

    if (shape[0] > 1 and shape[1] > 1):
        eps_x = 0.5 * (eps + np.roll(eps, -1, axis=0))
        eps_y = eps_y = 0.5 * (eps + np.roll(eps, -1, axis=1))
        eps_y = eps_y = 0.5 * (eps + np.roll(eps, -1))

        return eps_x, eps_y, eps_xy

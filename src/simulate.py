import matplotlib.pyplot as plt
from scipy.sparse.linalg import eigs
from scipy.linalg import eig, norm
import numpy as np
import numpy.matlib
import scipy.sparse as sp
from copy import deepcopy

# Import functions
from fdfd import fdfd


from util import *


def extend(eps, pad):

    eps = np.vstack((np.matlib.repmat(eps[0, :], pad[0], 1), eps, np.matlib.repmat(eps[-1, :], pad[1], 1)))
    eps = np.hstack((np.matlib.repmat(eps[:, 0].reshape(-1, 1), 1,  pad[2]),
                     eps, np.matlib.repmat(eps[:, -1].reshape(-1, 1), 1, pad[3])))

    return eps


# def my_project(ref, field):
#     def my_dot


def calc_power(Fxy, Fz, *args):

    # Calcualte output power
    Pout = np.sum(np.real(Fxy * np.conj(Fz)))

    # Calculate projection of mode
    # if args:
    #     mode = args[0]
    #     Fxy_mode = my_project(mode["xy"], Fxy)

    return Pout


# Temporary variables
spec = specs
pad = params["pad"]
cond_num = spec_wg["ncond"]
phase = np.zeros((cond_num))
# ------------
# Start here
# ------------

eps = extend(eps, pad)
mu = extend(mu, pad)

dims = eps.shape


# position of boundaries / interesting points
x_left = pad[0]
x_right = dims[0] - pad[1] - 1
x_top = np.arange(x_left, x_right + 1)
y_top = pad[2]
y_bot = dims[1] - pad[3] - 1
y_side = np.arange(y_top, y_bot + 1)
x_mon = x_right + spec[cond_num]["params"]["mon"]
y_mon = np.floor((y_top + y_bot)/2)
# y_mon

# Calcualte field
Fx, Fy, Fz = fdfd(spec, eps, mu, cond_num, pad, phase)


Pin = [None] * cond_num
Pright = [None] * cond_num
Pleft = [None] * cond_num
Ptop = [None] * cond_num
Pbot = [None] * cond_num
Pmon = [None] * cond_num
Pout = [None] * cond_num

for i in range(cond_num):
    Pin[i] = spec[i]["P_in"]
    Pright[i] = np.abs(calc_power(Fy[i][x_right, y_side], Fz[i][x_right, y_side]) / Pin[i] * 100)
    Pleft[i] = np.abs(calc_power(Fy[i][x_left, y_side], Fz[i][x_left, y_side]) / Pin[i] * 100)
    Ptop[i] = np.abs(calc_power(Fy[i][x_top, y_top], Fz[i][x_top, y_top]) / Pin[i] * 100)
    Pbot[i] = np.abs(calc_power(Fx[i][x_top, y_bot], Fz[i][x_top, y_bot]) / Pin[i] * 100)
    Pmon[i] = np.abs(calc_power(Fx[i][x_mon, y_side], Fz[i][x_mon, y_side]) / Pin[i] * 100)

    if (spec[i]["mode_type"] == 'TM'):
        mode = {"z": spec[i]["out"]["H"][:, 2]}

        if (spec[i]["out"]["side"] == 'left' or spec[i]["out"]["side"] == 'right'):
            mode["xy"] = spec[i]["out"]["E"][:, 1]
        else:
            mode["xy"] = spec[i]["out"]["E"][:, 0]

    # Calculate output power in desired mode
    # if (spec[i]["out"]["side"] == 'left'):
    #     Pout[i] = np.abs(calc_power(Fy[i][x_left, y_side], Fz[i][x_left, y_side], mode) / Pin[i] * 100)

# %%
# imshow(np.real(Fz[0]).T)
i = 0
Ptop

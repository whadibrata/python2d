import numpy as np
import scipy.sparse as sp

# Temp variables
dims = np.array([7, 7])
phase = np.array([0, 2*np.pi])
eps = np.ones(dims) * 2.3
top = 2
# eps = np.arange(49).reshape((5,5))

# Empty variables
TE_1, TE_2, TE_3, TE_4 = [], [], [], []


N = np.prod(dims)
C1 = np.ones((N, 1))
C2 = np.ones((N, 1))

for i in range(N):
    if (np.mod(i+1, dims[0]) == 1):
        C2[i, 0] = 0


for i in range(len(phase)):
    C3 = np.vstack((C2[1:], [0]))
    C4 = np.vstack((np.zeros((dims[0], 1)), np.ones((N - dims[0], 1))))
    C5 = np.vstack((np.ones((N - dims[0], 1)), np.zeros((dims[0], 1))))
    C6 = np.vstack((phase[i] * np.ones((dims[0], 1)), np.zeros((N - dims[0], 1))))
    C7 = np.vstack((np.zeros((N - dims[0], 1)), -phase[i] * np.ones((dims[0], 1))))

    DxE = sp.spdiags(np.hstack((-C1, C2)).T, np.array([0, 1]), N, N)
    DxH = sp.spdiags(np.hstack((-C3, C2)).T, np.array([-1, 0]), N, N)
    DyE = sp.spdiags(np.hstack((C6, -C1, C4)).T, np.array([-(N - dims[0]), 0, dims[0]]), N, N)
    DyH = sp.spdiags(np.hstack((-C5, C1, C7)).T, np.array([-dims[0], 0, (N - dims[0])]), N, N)

    epsX = sp.spdiags(0.5 * np.hstack((C1, C2)).T, np.array([0, 1]), N, N)
    epsY = sp.spdiags(0.5 * np.hstack((C1, C4)).T, np.array([0, dims[0]]), N, N)
    epsZ = sp.spdiags(C1.T, 0, N, N)

    muX = epsY
    muY = epsX
    muZ = 0.5 * (epsX + epsY)

    TE_1.append(sp.hstack((-DyH, DxH)))
    TE_3.append(sp.vstack((-DyH, DxH)))


TE_2.append(sp.vstack((muX, muY)))
TE_4.append(epsZ)

TE = [TE_1, TE_2, TE_3, TE_4]

eps_field = np.ones(dims)
x_field = np.ones(dims)
eps2 = eps
interior = True


def my_selection(dims, field, border):
    F = field.copy()
    F[border[0]:-border[0], border[1]:-border[1]] = 0
    field = field - F

    # Find interior and boundary
    ind_int = np.ravel_multi_index(np.where(field == 1), dims)
    ind_bnd = np.ravel_multi_index(np.where(field == 0), dims)

    # Create a sparse csr_matrix
    def my_sparse(ind): return sp.csr_matrix(
        (np.ones((ind.size)), (np.arange(ind.size), ind)), shape=(ind.size, np.prod(dims)))

    int = my_sparse(ind_int)
    bnd = my_sparse(ind_bnd)

    return int, bnd


def dbl(int, bnd):

    int2 = sp.vstack((sp.hstack((int, 0*int)), sp.hstack((0*int, int))))
    bnd2 = sp.vstack((sp.hstack((bnd, 0*bnd)), sp.hstack((0*bnd, bnd))))

    return int2, bnd2


S_int, S_bnd = my_selection(dims, eps_field, [2, top])
S_res, S_resbnd = my_selection(dims, eps_field, [1, int(top/2)])
Sx_int, Sx_bnd = my_selection(dims, x_field, [2, top])
Sx2_int, Sx2_bnd = dbl(Sx_int, Sx_bnd)
S2_res, S2_res2 = dbl(S_res, S_res)

S = {"int": S_int, "bnd": S_bnd, "res": S_res, "resbnd": S_resbnd}
Sx = {"int": Sx_int, "bnd": Sx_bnd}
Sx2 = {"int": Sx2_int, "bnd": Sx2_bnd}
S2 = {"res": S2_res, "res2": S_res}

# a = int2.todense()
# np.savetxt("foo.csv", a, delimiter=",")
